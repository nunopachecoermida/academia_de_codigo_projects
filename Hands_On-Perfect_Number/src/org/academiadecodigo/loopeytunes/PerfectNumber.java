package org.academiadecodigo.loopeytunes;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class PerfectNumber {

    public static void main(String[] args) {

        PerfectNumber pf = new PerfectNumber();
        long start = System.currentTimeMillis();

        pf.checkPerfects(1,200000);

        double elapsed = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - start);

        System.out.println(elapsed < 60d ? "time (secs): " + elapsed : "time (mins): "
                + TimeUnit.SECONDS.toMinutes((long) elapsed));
    }

    public int sumDivisors(int n) {
        return IntStream.range(1,n)
                .filter(x -> n % x == 0)
                .sum();
    }

    public void checkPerfects(int min, int max) {
        IntStream.range(min, max)
                .filter(n -> n == sumDivisors(n))
                .forEach(System.out::println);
    }
}
