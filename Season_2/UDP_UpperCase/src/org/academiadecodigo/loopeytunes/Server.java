package org.academiadecodigo.loopeytunes;

import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;


public class Server {

    public static void main(String[] args) {

        String hostName = "127.0.0.1";
        int serverPortNumber = 9000;
        byte[] recBuffer = new byte[1024];
        DatagramSocket socket = null;


        try {

            // RECEIVE
            socket = new DatagramSocket(serverPortNumber);
            DatagramPacket receivePacket = new DatagramPacket(recBuffer, recBuffer.length);

            socket.receive(receivePacket);

            String result = new String(recBuffer, 0, receivePacket.getLength());
            System.out.println(result);
            result = result.toUpperCase();

            // SEND
            byte[] sendBuffer = result.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length,
                    receivePacket.getAddress(), receivePacket.getPort());

            socket.send(sendPacket);


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (socket != null) {
                socket.close();
            }
        }


    }

}
