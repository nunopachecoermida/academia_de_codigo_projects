package org.academiadecodigo.loopeytunes;

import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;


public class Client {

    public static void main(String[] args) {

        String inputString = "Hello World!";
        byte[] sendBuffer = inputString.getBytes();
        DatagramSocket socket = null;

        try {

            // SEND

            socket = new DatagramSocket();

            DatagramPacket sendPacket = new DatagramPacket(
                    sendBuffer,
                    sendBuffer.length,
                    InetAddress.getByName("127.0.0.1"),
                    9000);

            socket.send(sendPacket);

            // RECEIVE

            byte[] recBuffer = new byte[1024];

            DatagramPacket receivePacket = new DatagramPacket(recBuffer, recBuffer.length);
            socket.receive(receivePacket);

            String result = new String(recBuffer, 0, receivePacket.getLength());

            System.out.println(result);

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException exception) {
            exception.printStackTrace();
        } finally {
            if ( socket != null) {
                socket.close();
            }
        }

    }
}
