package org.academiadecodigo.bootcamp.concurrency;

public class Plate {

    private String nameOfPlate;

    public Plate(String nameOfPlate) {
        this.nameOfPlate = nameOfPlate;
    }

    public String getNameOfPlate() {
        return nameOfPlate;
    }
}
