package org.academiadecodigo.bootcamp.concurrency.bqueue;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.SynchronousQueue;

/**
 * Blocking Queue
 *
 * @param <Plate> the type of elements stored by this queue
 */
public class BQueue<Plate> {

    private Queue<Plate> queue;
    private int queueLimit;
    private Object lock = new Object();

    public String getNameOfQueue() {
        return nameOfQueue;
    }

    private String nameOfQueue;

    /**
     * Constructs a new queue with a maximum size
     *
     * @param limit the queue size
     */
    public BQueue(String nameOfQueue, int limit) {
        this.nameOfQueue = nameOfQueue;
        queueLimit = limit;
        queue = new LinkedList<Plate>();

    }

    /**
     * Inserts the specified element into the queue
     * Blocking operation if the queue is full
     *
     * @param plate the data to add to the queue
     */
    public synchronized void offer(Plate plate) {

            while (getSize() == getLimit()) {
                System.out.println("## QUEUE IS FULL, ELEMENTS MUST BE REMOVED ##");
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            queue.offer(plate);
            notifyAll();
    }

    /**
     * Retrieves and removes data from the head of the queue
     * Blocking operation if the queue is empty
     *
     * @return the data from the head of the queue
     */
    public synchronized Plate poll() {

            while (queue.isEmpty()) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            notifyAll();
            return queue.poll();

    }

    /**
     * Gets the number of elements in the queue
     *
     * @return the number of elements
     */
    public int getSize() {

        return queue.size();

    }

    /**
     * Gets the maximum number of elements that can be present in the queue
     *
     * @return the maximum number of elements
     */
    public int getLimit() {

        return queueLimit;

    }

}
