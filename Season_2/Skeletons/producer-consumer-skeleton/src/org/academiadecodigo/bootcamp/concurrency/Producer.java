package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;

/**
 * Produces and stores integers into a blocking queue
 */
public class Producer implements Runnable {

    private final BQueue<Plate> queue;
    private int elementNum;
    private Plate plateOfFood = new Plate("Jardineira");
    private String name;

    /**
     * @param queue      the blocking queue to add elements to
     * @param elementNum the number of elements to produce
     */
    public Producer(String name, BQueue queue, int elementNum) {
        this.name = name;
        this.queue = queue;
        this.elementNum = elementNum;
    }

    @Override
    public void run() {

        int currentElement = 0;

        while (currentElement < elementNum) {

            try {
                Thread.sleep((long) (Math.random() * 200));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (queue) {
                currentElement++;
                queue.offer(plateOfFood);
                System.out.println(name + ": Element added. I've already added " + currentElement + " elements");
                System.out.println("Number of elements is "
                        + queue.getSize());
            }
        }

        System.out.println(name + ": No more elements to be added to the queue because I already added " +
                queue.getLimit() + " elements");

        if (queue.getSize() == queue.getLimit()) {
            System.out.println("## QUEUE IS FULL, PLEASE REMOVE ELEMENT ##");
        }

    }

}
