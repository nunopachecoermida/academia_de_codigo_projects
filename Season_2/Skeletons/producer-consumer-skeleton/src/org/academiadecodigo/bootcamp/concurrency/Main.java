package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;

public class Main {

    public static void main(String[] args) {

        BQueue<Integer> queue = new BQueue<>("TAPETE", 10);

        Plate plate = new Plate("Jardineira");

        System.out.println("A queue was created with a maximum of " + queue.getLimit() + " elements\n");


        Producer p1 = new Producer("Produtor 1", queue, 100);
        Thread t1 = new Thread(p1);
        t1.setName("p1");

        Producer p2 = new Producer("Produtor 2", queue, 100);
        Thread t2 = new Thread(p2);
        t2.setName("p2");

        Consumer c1 = new Consumer("Consumer 1", queue, 80);
        Thread t3 = new Thread(c1);
        t3.setName("c1");

        Consumer c2 = new Consumer("Consumer 2", queue, 120);
        Thread t4 = new Thread(c2);
        t4.setName("c2");

        t3.start();
        t4.start();

        t1.start();

        t2.start();

    }

}

