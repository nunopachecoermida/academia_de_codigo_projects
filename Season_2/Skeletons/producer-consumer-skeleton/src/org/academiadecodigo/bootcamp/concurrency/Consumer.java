package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;

/**
 * Consumer of integers from a blocking queue
 */
public class Consumer implements Runnable {

    private final BQueue<Plate> queue;
    private int elementNum;
    private String name;

    /**
     * @param queue      the blocking queue to consume elements from
     * @param elementNum the number of elements to consume
     */
    public Consumer(String name, BQueue queue, int elementNum) {
        this.name = name;
        this.queue = queue;
        this.elementNum = elementNum;
    }


    public void run() {

        int elementRemoved = 0;


        while (elementRemoved < elementNum) {

            try {
                Thread.sleep((long) (Math.random() * 200));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (queue) {
                elementRemoved++;
                queue.poll();
                System.out.println(name + ": Consuming.");
                System.out.println("Number of elements is: " + queue.getSize());
                if (queue.getSize() == 0) {
                    System.out.println("## QUEUE IS EMPTY, PLEASE FILL IT ##");
                }
            }


        }
        System.out.println("No more elements to be removed");
    }
}

