package org.academiadecodigo.loopeytunes;

public class TrabalhadorDasCaldas implements Runnable {

    private int number;

    public TrabalhadorDasCaldas(int number) {

        this.number = number;
    }

    public void work() {
        System.out.println(Thread.currentThread().getName());
        for (int i = 0; i < 10; i++) {
            System.out.println("Trabalhador " + number + ": Fazendo aqui uma loiça");
        }
    }

    @Override
    public void run() {
        work();
    }

}
