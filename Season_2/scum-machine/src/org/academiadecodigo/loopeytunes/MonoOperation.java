package org.academiadecodigo.loopeytunes;

public interface MonoOperation<T> {

    T operate(T num2);
}
