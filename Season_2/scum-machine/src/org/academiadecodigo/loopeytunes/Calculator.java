package org.academiadecodigo.loopeytunes;

import java.util.function.BiFunction;
import java.util.function.Function;

public class Calculator {

    public <T> T doOperation(T operand1, T operand2, BiFunction<T, T, T> operation) {
        return operation.apply(operand1, operand2);
    }
}
