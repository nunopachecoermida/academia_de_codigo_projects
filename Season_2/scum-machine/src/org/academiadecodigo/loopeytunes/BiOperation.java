package org.academiadecodigo.loopeytunes;

public interface BiOperation<T> {

    T operate(T num1, T num2);
}
