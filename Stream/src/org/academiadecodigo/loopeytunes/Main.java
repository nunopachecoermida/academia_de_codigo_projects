package org.academiadecodigo.loopeytunes;

import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        String original = "I'll send an SOS to the garbage world, " + "I hope that someone garbage gets my message in a garbage bottle.";

        String newstring = Stream.of(original.split(" "))
                .filter(word -> !word.equals("garbage"))
                .map(String::toUpperCase)
                .reduce("", (result, word2) -> result + word2 + " ");

        System.out.println(newstring);
    }

}
