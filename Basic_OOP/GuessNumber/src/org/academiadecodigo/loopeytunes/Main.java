package org.academiadecodigo.loopeytunes;

public class Main {

    public static void main(String[] args) {

        Player[] players = new Player[]{
                new Player("Nuno"),
                new Player("Victoria"),
                new Player("Isa"),
                new Player("Juju")
        };

        GuessNumber game = new GuessNumber(10, players);
        game.start();

    }
}
