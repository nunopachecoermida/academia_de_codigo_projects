package org.academiadecodigo.loopeytunes;

public class ClosestNeighbours {

    public static void main(String[] args) {

        int[] myArray = {0, 5, 1209, 6, 2, 111, 112, 33};
        int[] result = findClosest(myArray);

        System.out.println(result[0] + " and " +  result[1]);

    }


    private static int[] findClosest(int[] numbers) {

        int difference;
        int minDifference = Math.abs((numbers[0] - numbers[1]));
        int[] neighbours = new int[2];

        for (int i = 0; i < numbers.length; i++) {

            difference = Math.abs((numbers[i] - numbers[i + 1]));

            if (difference <= minDifference) {

                minDifference = difference;
                neighbours[0] = numbers[i];
                neighbours[1] = numbers[i + 1];
            }
        }
        return neighbours;

    }
}