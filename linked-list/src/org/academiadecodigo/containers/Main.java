package org.academiadecodigo.containers;

public class Main {

    public static void main(String[] args) {

        // Adding elements to the initial list
        LinkedList<Integer> myList = new LinkedList<>();

        for (int i = 0; i <= 10; i++) {
            myList.add(i);
            System.out.println(myList.get(i));
        }

        // Filtering the list

        LinkedList<Integer> filteredList = myList.filter((e) -> e > 5);

        System.out.println("\n");

        for (int i = 0; i < filteredList.size(); i++) {
            System.out.println(filteredList.get(i));
        }

        System.out.println("\n");

        // Mapping the list (transforming each element)

        LinkedList<Integer> mappedList = filteredList.map((e) -> e + 10);

        for (int i = 0; i < mappedList.size(); i++) {
            System.out.println(mappedList.get(i));
        }






    }
}
